﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBAdminTool.WindowObjects
{
    public class ItemDetail
    {
        public string Name { get; set; }
        public string GUID { get; set; }
    }
}
