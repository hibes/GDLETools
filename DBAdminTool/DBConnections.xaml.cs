﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DBAdminTool
{
    /// <summary>
    /// Interaction logic for DBConnections.xaml
    /// </summary>
    public partial class DBConnections : Window
    {
        public DBConnections()
        {
            InitializeComponent();
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            connectionServerAddr.Focus();
        }

        private void ConnectionTest_Click(object sender, RoutedEventArgs e)
        {
            if (VerifyInputs())
            {
                string connectionString = CreateConnectionString();
                //connectionString = "server=127.0.0.1:3306;Uid=root;Pwd=ACRul3Z!;Database=phatac;";
                //connectionString = "server=127.0.0.1;port=3306;user id=root;password=ACRul3Z!;database=phatac;ignoreprepare=False;pooling=True;allowuservariables=True;allowzerodatetime=True";
                using (MySqlConnection conn = new MySqlConnection(connectionString))
                {
                    try
                    {
                        conn.Open();
                        connectionTestResult.Content = "Connection successful";
                        Application.Current.Properties["dbconnectionvalid"] = "true";
                        Application.Current.Properties["dbconnectionstring"] = connectionString;
                    }
                    catch (MySqlException ex)
                    {
                        switch (ex.Number)
                        {
                            case 1045:
                                connectionTestResult.Content = "Invalid username/password";
                                break;
                            default:
                                connectionTestResult.Content = "Cannot connect to server.";
                                break;
                        }
                    }
                }
            }
        }

        private bool VerifyInputs()
        {
            if (connectionServerAddr.Text.IsNullOrEmpty())
            {
                ShowError("Server Address cannot be blank");
                return false;
            }

            if (connectionServerPort.Text.IsNullOrEmpty())
            {
                ShowError( "Server Port cannot be blank");
                return false;
            }

            if (!Int32.TryParse(connectionServerPort.Text.Trim(), out int portcheck))
            {
                ShowError("Server Port must be between 1 & 64000");
            }

            if (connectionServerUsername.Text.IsNullOrEmpty())
            {
                ShowError("Username cannot be blank");
                return false;
            }

            if (connectionServerPassword.Text.IsNullOrEmpty())
            {
                ShowError("Password cannot be blank");
                return false;
            }

            if (connectionServerDatabase.Text.IsNullOrEmpty())
            {
                ShowError("Database name cannot be blank");
                return false;
            }

            return true;
        }

        private void ShowError(string errorMsg)
        {
            MessageBox.Show(errorMsg, "Connection Test Error", MessageBoxButton.OK);
        }

        private string CreateConnectionString()
        {
            return new MySqlConnectionStringBuilder()
            {
                Server = connectionServerAddr.Text.Trim(),
                Port = Convert.ToUInt32(connectionServerPort.Text.Trim()),
                UserID = connectionServerUsername.Text.Trim(),
                Password = connectionServerPassword.Text.Trim(),
                Database = connectionServerDatabase.Text.Trim(),
                IgnorePrepare = false,
                Pooling = true,
                AllowUserVariables = true,
                AllowZeroDateTime = true
            }.ToString();
        }

        private void ConnectionClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            Application.Current.MainWindow.Focus();
        }
    }
}
