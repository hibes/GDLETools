﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBAdminTool.Objects
{
    public class GeneratorQueueNode : IPackable<GeneratorQueueNode>
    {
        public uint Slot { get; private set; }
        public double When { get; private set; }

        public GeneratorQueueNode()
        { }

        public GeneratorQueueNode Unpack(BinaryReader reader)
        {
            Slot = reader.ReadUInt32();
            When = reader.ReadDouble();
            return this;
        }

        public void Pack(BinaryWriter writer)
        {
            writer.Write(Slot);
            writer.Write(When);
        }
    }
}
