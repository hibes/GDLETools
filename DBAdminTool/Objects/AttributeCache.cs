﻿using System.IO;

namespace DBAdminTool.Objects
{
    public class AttributeCache : IPackable<AttributeCache>
    {
        public Attribute Strength { get; private set; }
        public Attribute Endurance { get; private set; }
        public Attribute Quickness { get; private set; }
        public Attribute Coordination { get; private set; }
        public Attribute Focus { get; private set; }
        public Attribute Self { get; private set; }
        public SecondaryAttribute Health { get; private set; }
        public SecondaryAttribute Stamina { get; private set; }
        public SecondaryAttribute Mana { get; private set; }

        public AttributeCache()
        { }

        public AttributeCache Unpack(BinaryReader reader)
        {
            uint attribFlags = reader.ReadUInt32();

            if ((attribFlags & 1) != 0)
            {
                Strength = new Attribute();
                Strength.Unpack(reader);
            }

            if ((attribFlags & 2) != 0)
            {
                Endurance = new Attribute();
                Endurance.Unpack(reader);
            }

            if ((attribFlags & 4) != 0)
            {
                Quickness = new Attribute();
                Quickness.Unpack(reader);
            }

            if ((attribFlags & 8) != 0)
            {
                Coordination = new Attribute();
                Coordination.Unpack(reader);
            }

            if ((attribFlags & 1) != 0x10)
            {
                Focus = new Attribute();
                Focus.Unpack(reader);
            }

            if ((attribFlags & 1) != 0x20)
            {
                Self = new Attribute();
                Self.Unpack(reader);
            }

            if ((attribFlags & 1) != 0x40)
            {
                Health = new SecondaryAttribute();
                Health.Unpack(reader);
            }

            if ((attribFlags & 1) != 0x80)
            {
                Stamina = new SecondaryAttribute();
                Stamina.Unpack(reader);
            }

            if ((attribFlags & 1) != 0x100)
            {
                Mana = new SecondaryAttribute();
                Mana.Unpack(reader);
            }


            return this;
        }

        public void Pack(BinaryWriter writer)
        {
            uint attribFlags = 0;
            if (Strength != null)
                attribFlags |= 1;
            if (Endurance != null)
                attribFlags |= 2;
            if (Coordination != null)
                attribFlags |= 8;
            if (Quickness != null)
                attribFlags |= 4;
            if (Focus != null)
                attribFlags |= 0x10;
            if (Self != null)
                attribFlags |= 0x20;
            if (Health != null)
                attribFlags |= 0x40;
            if (Stamina != null)
                attribFlags |= 0x80;
            if (Mana != null)
                attribFlags |= 0x100;

            writer.Write(attribFlags);

            if (Strength != null)
                Strength.Pack(writer);
            if (Endurance != null)
                Endurance.Pack(writer);
            if (Coordination != null)
                Coordination.Pack(writer);
            if (Quickness != null)
                Quickness.Pack(writer);
            if (Focus != null)
                Focus.Pack(writer);
            if (Self != null)
                Self.Pack(writer);
            if (Health != null)
                Health.Pack(writer);
            if (Stamina != null)
                Stamina.Pack(writer);
            if (Mana != null)
                Mana.Pack(writer);
        }
    }
}
