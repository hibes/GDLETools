﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBAdminTool.Objects
{
    public class GeneratorTable : IPackable<GeneratorTable>
    {
        public List<GeneratorProfile> Profiles { get; private set; }

        public GeneratorTable()
        {
            Profiles = new List<GeneratorProfile>();
        }

        public GeneratorTable Unpack(BinaryReader reader)
        {
            ushort numGenProfiles = reader.ReadUInt16();
            ushort sizeGenProfile = reader.ReadUInt16();

            for (int gp = 0; gp < numGenProfiles; gp++)
            {
                Profiles.Add(new GeneratorProfile().Unpack(reader));
            }
            return this;
        }

        public void Pack(BinaryWriter writer)
        {
            writer.Write((ushort)Profiles.Count);
            writer.Write((ushort)16);

            foreach (GeneratorProfile gp in Profiles)
                gp.Pack(writer);
        }
    }
}
