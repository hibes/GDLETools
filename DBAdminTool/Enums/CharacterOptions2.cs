﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBAdminTool.Enums
{
    public enum CharacterOptions2
    {
        Undef_CharacterOptions2 = 0x0,
        PersistentAtDay_CharacterOptions2 = 0x1,
        DisplayDateOfBirth_CharacterOptions2 = 0x2,
        DisplayChessRank_CharacterOptions2 = 0x4,
        DisplayFishingSkill_CharacterOptions2 = 0x8,
        DisplayNumberDeaths_CharacterOptions2 = 0x10,
        DisplayAge_CharacterOptions2 = 0x20,
        TimeStamp_CharacterOptions2 = 0x40,
        SalvageMultiple_CharacterOptions2 = 0x80,
        HearGeneralChat_CharacterOptions2 = 0x100,
        HearTradeChat_CharacterOptions2 = 0x200,
        HearLFGChat_CharacterOptions2 = 0x400,
        HearRoleplayChat_CharacterOptions2 = 0x800,
        AppearOffline_CharacterOptions2 = 0x1000,
        DisplayNumberCharacterTitles_CharacterOptions2 = 0x2000,
        MainPackPreferred_CharacterOptions2 = 0x4000,
        LeadMissileTargets_CharacterOptions2 = 0x8000,
        UseFastMissiles_CharacterOptions2 = 0x10000,
        FilterLanguage_CharacterOptions2 = 0x20000,
        ConfirmVolatileRareUse_CharacterOptions2 = 0x40000,
        HearSocietyChat_CharacterOptions2 = 0x80000,
        ShowHelm_CharacterOptions2 = 0x100000,
        DisableDistanceFog_CharacterOptions2 = 0x200000,
        UseMouseTurning_CharacterOptions2 = 0x400000,
        ShowCloak_CharacterOptions2 = 0x800000,
        LockUI_CharacterOptions2 = 0x1000000,
        Default_CharacterOptions2 = 0x948700,
        FORCE_CharacterOptions2_32_BIT = 0x7FFFFFFF,
    };

}
