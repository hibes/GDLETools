﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBAdminTool.Enums
{
    public enum PropertyIID
    {
        UNDEF_IID,
        OWNER_IID,
        CONTAINER_IID,
        WIELDER_IID,
        FREEZER_IID,
        VIEWER_IID,
        GENERATOR_IID,
        SCRIBE_IID,
        CURRENT_COMBAT_TARGET_IID,
        CURRENT_ENEMY_IID,
        PROJECTILE_LAUNCHER_IID,
        CURRENT_ATTACKER_IID,
        CURRENT_DAMAGER_IID,
        CURRENT_FOLLOW_TARGET_IID,
        CURRENT_APPRAISAL_TARGET_IID,
        CURRENT_FELLOWSHIP_APPRAISAL_TARGET_IID,
        ACTIVATION_TARGET_IID,
        CREATOR_IID,
        VICTIM_IID,
        KILLER_IID,
        VENDOR_IID,
        CUSTOMER_IID,
        BONDED_IID,
        WOUNDER_IID,
        ALLEGIANCE_IID,
        PATRON_IID,
        MONARCH_IID,
        COMBAT_TARGET_IID,
        HEALTH_QUERY_TARGET_IID,
        LAST_UNLOCKER_IID,
        CRASH_AND_TURN_TARGET_IID,
        ALLOWED_ACTIVATOR_IID,
        HOUSE_OWNER_IID,
        HOUSE_IID,
        SLUMLORD_IID,
        MANA_QUERY_TARGET_IID,
        CURRENT_GAME_IID,
        REQUESTED_APPRAISAL_TARGET_IID,
        ALLOWED_WIELDER_IID,
        ASSIGNED_TARGET_IID,
        LIMBO_SOURCE_IID,
        SNOOPER_IID,
        TELEPORTED_CHARACTER_IID,
        PET_IID,
        PET_OWNER_IID,
        PET_DEVICE_IID,

        NUM_IID_STAT_VALUES
    };

}
