﻿using DBAdminTool.Objects;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Windows;

namespace DBAdminTool
{
    /// <summary>
    /// Interaction logic for CharacterSelect.xaml
    /// </summary>
    public partial class CharacterSelect : Window
    {
        public event EventHandler CharacterSelected;

        
        public Weenie ChildWeenie { get; set; }

        public CharacterSelect()
        {
            InitializeComponent();
            LoadCharacters();
        }

        private void LoadCharacters()
        {
            List<string> characterNames = new List<string>();

            characterNames = DBUtils.GetCharacters(Application.Current.Properties["dbconnectionstring"].ToString());
            characterselectlist.ItemsSource = characterNames;

        }

        public CharacterSelect(List<string> characterNames)
        {
            InitializeComponent();
            characterselectlist.ItemsSource = characterNames;
        }

        private void Charcterselectedit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Properties["selectedcharacter"] = characterselectlist.SelectedItem.ToString();

            DataTable dataTable = DBUtils.GetCharacterData(Application.Current.Properties["selectedcharacter"], Application.Current.Properties["dbconnectionstring"].ToString());
            uint weenieID = Convert.ToUInt32(dataTable.Rows[0][0]);
            Byte[] charData = (byte[])dataTable.Rows[0][1];
            string loadFile = "d:\\acstuff\\" + Application.Current.Properties["selectedcharacter"] + "_OnLoad.txt";
            File.WriteAllBytes(loadFile, charData);
            if (charData.Length == 0)
            {
                MessageBox.Show($"Missing Character Data row for {Application.Current.Properties["selectedcharacter"]}");
                Close();
            }
            else
            {
                using (BinaryReader br = new BinaryReader(new MemoryStream(charData)))
                {
                    ChildWeenie = new Weenie(weenieID).Unpack(br);
                }
                DataTable inventory = DBUtils.GetCharInventory(Application.Current.Properties["selectedcharacter"], Application.Current.Properties["dbconnectionstring"].ToString());
                foreach (DataRow dr in inventory.Rows)
                {
                    byte[] invItem = (byte[])dr[1];
                   
                    using (BinaryReader ibr = new BinaryReader(new MemoryStream(invItem)))
                    {
                        ChildWeenie.LoadInventory(Convert.ToUInt32(dr[0].ToString().Trim()), ibr);
                        loadFile = "d:\\acstuff\\" + ChildWeenie.WeenieID + "_OnLoad.txt";
                        File.WriteAllBytes(loadFile, invItem);
                    }
                }
                ChildWeenie.LoadPacks(inventory);
            }

            CharacterSelected?.Invoke(this, new EventArgs());

            CharacterEditor characterEditor = new CharacterEditor(ChildWeenie);
            characterEditor.Show();

            Close();


        }


    }
}
