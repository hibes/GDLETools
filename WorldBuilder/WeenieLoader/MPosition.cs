﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Weenies.Types
{
    public partial class MPosition
    {
        [JsonProperty("frame")]
        public Frame Frame { get; set; }

        [JsonProperty("objcell_id")]
        public long ObjcellId { get; set; }
    }
}
