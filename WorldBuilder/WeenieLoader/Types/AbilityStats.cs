using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weenies.Types
{
    public class StatBase
    {
        [JsonProperty("cp_spent")]
        public long CpSpent { get; set; }

        [JsonProperty("init_level")]
        public long InitLevel { get; set; }

        [JsonProperty("level_from_cp")]
        public long LevelFromCp { get; set; }
    }

    public class StatVolitile : StatBase
    {
        [JsonProperty("current")]
        public long Current { get; set; }
    }
}
