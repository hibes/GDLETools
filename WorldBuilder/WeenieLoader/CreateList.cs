using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weenies.Types
{
    public class CreateList
    {
        [JsonProperty("destination")]
        public long Destination { get; set; }

        [JsonProperty("palette")]
        public long Palette { get; set; }

        [JsonProperty("shade")]
        public long Shade { get; set; }

        [JsonProperty("stack_size")]
        public long StackSize { get; set; }

        [JsonProperty("try_to_bond")]
        public long TryToBond { get; set; }

        [JsonProperty("wcid")]
        public long Wcid { get; set; }
    }
}
