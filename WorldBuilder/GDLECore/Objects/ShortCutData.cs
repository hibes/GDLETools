﻿using GDLECore.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDLECore.Objects
{
    public class ShortCutData : IPackable<ShortCutData>
    {
        public uint Index { get; private set; }
        public uint ObjectID { get; private set; }
        public uint SpellID { get; private set; }

        public ShortCutData()
        { }

        public ShortCutData Unpack(BinaryReader reader)
        {
            Index = reader.ReadUInt32();
            ObjectID = reader.ReadUInt32();
            SpellID = reader.ReadUInt32();

            return this;
        }

        public void Pack(BinaryWriter writer)
        {
            writer.Write(Index);
            writer.Write(ObjectID);
            writer.Write(SpellID);
        }
    }
}
