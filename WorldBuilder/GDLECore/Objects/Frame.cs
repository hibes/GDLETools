﻿using System.IO;
using GDLECore.Interfaces;

namespace GDLECore.Objects
{
    public class Frame : AFrame, IPackable<Frame>
    {
        public double[,] Matrix = new double[3, 3];

        public Frame()
        { }

        public new Frame Unpack(BinaryReader reader)
        {
            base.Unpack(reader);
            return this;
        }

        public new void Pack(BinaryWriter writer)
        {
            base.Pack(writer);
        }

    }
}
