﻿using GDLECore.Enums;
using GDLECore.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDLECore.Objects
{
    //public class CraftRequirement<T, U> : IPackable<CraftRequirement<T, U>> where T: Enum 
    public class CraftMod<T, U> : IPackable<CraftMod<T, U>> where T : Enum
    {
        public int UnknownInt;
        public int Operation; // TODO: Make enum
        public T Property;
        public U Value;

        public void Pack(BinaryWriter writer)
        {
            throw new NotImplementedException();
        }

        public CraftMod<T, U> Unpack(BinaryReader reader)
        {
            T p = (T)Enum.Parse(typeof(T), Property.ToString());


            return this;
        }
    }
}
