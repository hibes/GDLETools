﻿using GDLECore.Enums;
using GDLECore.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDLECore.Objects
{
    public class StringCraftMod : IPackable<StringCraftMod>
    {
        public int UnknownInt;
        public int Operation; // TODO: Make enum
        public PropertyString Property;
        public string Value;

        public void Pack(BinaryWriter writer)
        {
            throw new NotImplementedException();
        }

        public StringCraftMod Unpack(BinaryReader reader)
        {
            UnknownInt = reader.ReadInt32();
            Operation = reader.ReadInt32();
            Property = (PropertyString)reader.ReadInt32();
            Value = reader.ReadGDLEString();
            return this;
        }
    }
}
