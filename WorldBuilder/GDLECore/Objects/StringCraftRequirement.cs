﻿using GDLECore.Enums;
using GDLECore.Interfaces;
using System;
using System.IO;

namespace GDLECore.Objects
{
    public class StringCraftRequirement : IPackable<StringCraftRequirement>
    {
        public PropertyString Property;
        public string Value;
        public uint Operation;  // TODO: Change to enum
        public string Message;

        public void Pack(BinaryWriter writer)
        {
            throw new NotImplementedException();
        }

        public StringCraftRequirement Unpack(BinaryReader reader)
        {
            Property = (PropertyString)reader.ReadUInt32();
            Value = reader.ReadGDLEString();
            Operation = reader.ReadUInt32();
            Message = reader.ReadGDLEString();

            return this;
        }
    }
}
