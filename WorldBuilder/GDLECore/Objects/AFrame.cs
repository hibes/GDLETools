﻿using System.IO;
using GDLECore.Interfaces;


namespace GDLECore.Objects
{
    public class AFrame : IPackable<AFrame>
    {
        public Vector Origin { get; private set; }
        public Quaternion Quaternion { get; private set; }

        public AFrame()
        { }

        public AFrame Unpack(BinaryReader reader)
        {
            Origin = new Vector().Unpack(reader);
            Quaternion = new Quaternion().Unpack(reader);
            return this as AFrame;
        }

        public void Pack(BinaryWriter writer)
        {
            Origin.Pack(writer);
            Quaternion.Pack(writer);
        }
    }
}
