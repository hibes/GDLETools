﻿using System.IO;
using GDLECore.Interfaces;

namespace GDLECore.Objects
{
    public class AnimPartChange : IPackable<AnimPartChange>
    {
        private uint defaultMask = 0x01000000;
        public uint PartIndex { get; private set; }
        public uint PartId { get; private set; }

        public AnimPartChange()
        { }

        public AnimPartChange Unpack(BinaryReader reader)
        {
            PartIndex = reader.ReadByte();
            PartId = reader.UnpackFromUnknown(defaultMask);
            return this;
        }

        public void Pack(BinaryWriter writer)
        {
            writer.Write((ushort)PartIndex);
            writer.PackToUnknown(defaultMask, PartId);
        }

    }
}
