﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CachePwnLoader
{
    public enum BinType
    {
        Region,
        Spell,
        Treasure,
        Crafting,
        HousePortal,
        LandblockExtendedData,
        Jumpsuit,
        Quest,
        Weenie,
        Mutation,
        GameEvent
    }
}
