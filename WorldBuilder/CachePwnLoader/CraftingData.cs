﻿using GDLECore.Objects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CachePwnLoader
{
    public class CraftingData
    {
        public static CraftTable CraftTable;
        
        public CraftingData()
        {
            if (string.IsNullOrEmpty(Common.PathToCraftBin) || !File.Exists(Common.PathToCraftBin))
                throw new NullReferenceException("Path to crafting bin not valid");

            ParseBin();
        }

        private void ParseBin()
        {
            using (BinaryReader br = new BinaryReader(new FileStream(Common.PathToCraftBin, FileMode.Open)))
            {
                CraftTable = new CraftTable().Unpack(br);
            }
        }

    }
}
