﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GDLECore;
using GDLECore.Data;

namespace WeenieFlush5000
{
    public class Flusher
    {
        readonly string serverIP = string.Empty;
        readonly string serverPort = string.Empty;
        readonly string serverUser = string.Empty;
        readonly string serverPass = string.Empty;
        readonly string serverdbname = string.Empty;
        Dictionary<uint, string> characters;
        List<uint> badIds;

        const int timeDiff = 86400 * 3;

        public Flusher(string dbServerIp, string dbServerPort, string dbServerUser, string dbServerPass, string dbname)
        {
            serverIP = dbServerIp;
            serverPort = dbServerPort;
            serverUser = dbServerUser;
            serverPass = dbServerPass;
            serverdbname = dbname;
            characters = new Dictionary<uint, string>();
            badIds = new List<uint>();

            if (!IsDatabaseConnectionValid())
                throw new ApplicationException("Unable to access database");
        }

        public bool LoadCharacterList()
        {
            try
            {
                characters = Database.DBUtils.GetCharIdsAndNames();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return false;
            }
            return true;
        }

        public void CheckCharacterSavePattern(bool fix = false)
        {
            using (var f = new StreamWriter("extradatachars.csv"))
            {
                var headerline = string.Format("character,rows,badrows");
                f.WriteLine(headerline);
                f.Flush();
                foreach (KeyValuePair<uint, string> character in characters)
                {
                    int countOfBadRows = 0;
                    // Get Char row
                    DataTable CharacterInfo = Database.DBUtils.GetCharacterData(character.Key);
                    if (CharacterInfo.Rows.Count == 0)
                        continue;
                    Byte[] charData = (byte[])CharacterInfo.Rows[0][1];
                    // Get date of char row
                    Weenie CharWeenie;
                    using (BinaryReader br = new BinaryReader(new MemoryStream(charData)))
                    {
                        CharWeenie = new Weenie(character.Key).Unpack(br);
                    }
                    // Get mapped rows
                    DataTable charInventory = Database.DBUtils.GetCharInventory(character.Key);
                    foreach (DataRow dr in charInventory.Rows)
                    {
                        Byte[] thisrowdata = (byte[])dr[1];
                        Weenie weenie;
                        using (BinaryReader brdr = new BinaryReader(new MemoryStream(thisrowdata)))
                        {
                            weenie = new Weenie(Convert.ToUInt32(dr[0].ToString().Trim())).Unpack(brdr);
                        }
                        if (weenie.SaveTimestamp < CharWeenie.SaveTimestamp - timeDiff)
                        {
                            badIds.Add(weenie.WeenieID);
                            countOfBadRows++;
                        }
                    }

                    if (countOfBadRows > 0)
                    {
                        var line = string.Format("{0},{1},{2}", character.Value, charInventory.Rows.Count, countOfBadRows);
                        //Console.WriteLine(line);
                        f.WriteLine(line);
                        f.Flush();

                    }


                }
            }

            using (var f = new StreamWriter("idstodelete.csv"))
            {
                foreach (uint rowToGo in badIds)
                {
                    f.WriteLine(rowToGo);
                    f.Flush();
                }
            }

            if (fix)
                CleanDatabaseOfBadRows();
        }

        public void CleanDatabaseOfBadRows()
        {
            foreach (uint rowToGo in badIds)
            {
                Database.DBUtils.DeleteWeenieById(rowToGo);
            }
        }

        private bool IsDatabaseConnectionValid()
        {
            try
            {
                Database.DBUtils.ConnectAndOpen(serverIP, serverPort, serverUser, serverPass, serverdbname);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return false;
            }
            finally
            {
                Database.DBUtils.CloseDB();
            }

            return true;
        }


    }
}
