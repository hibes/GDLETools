﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeenieFlush5000
{
    class Program
    {
        static void Main(string[] args)
        {
            Flusher flusher = new Flusher(ConfigurationManager.AppSettings["server"], ConfigurationManager.AppSettings["port"],
                ConfigurationManager.AppSettings["user"], ConfigurationManager.AppSettings["pass"], ConfigurationManager.AppSettings["dbname"]);

            if (flusher.LoadCharacterList())
            {
                flusher.CheckCharacterSavePattern(Convert.ToBoolean(ConfigurationManager.AppSettings["cleanchartimestamprows"]));
            }

        }
    }
}
