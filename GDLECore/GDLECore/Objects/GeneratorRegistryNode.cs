﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDLECore
{
    public class GeneratorRegistryNode : IPackable<GeneratorRegistryNode>
    {
        public uint WClassIdOrTreasureType { get; private set; }
        public double TS { get; private set; }
        public uint TreasureType { get; private set; }
        public uint Slot { get; private set; }
        public int Checkpointed { get; private set; }
        public int Shop { get; private set; }
        public int Amount { get; private set; }

        public uint ObjectID { get; private set; }

        public GeneratorRegistryNode()
        { }

        public GeneratorRegistryNode Unpack(BinaryReader reader)
        {
            WClassIdOrTreasureType = reader.ReadUInt32();
            TS = reader.ReadDouble();
            TreasureType = reader.ReadUInt32();
            Slot = reader.ReadUInt32();
            Checkpointed = reader.ReadInt32();
            Shop = reader.ReadInt32();
            Amount = reader.ReadInt32();
            return this;
        }

        public void Pack(BinaryWriter writer)
        {
            writer.Write(WClassIdOrTreasureType);
            writer.Write(TS);
            writer.Write(TreasureType);
            writer.Write(Slot);
            writer.Write(Checkpointed);
            writer.Write(Shop);
            writer.Write(Amount);
        }
    }
}
