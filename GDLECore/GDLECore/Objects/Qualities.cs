﻿
using GDLECore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDLECore
{
    public class Qualities : BaseQualities, IPackable<Qualities>
    {
        public AttributeCache AttributeCache { get; private set; }
        public Dictionary<SkillList, Skill> Skills { get; private set; }

        public Body Body { get; private set; }
        public SpellBook SpellBook { get; private set; }
        public EnchantmentRegistry Enchantments { get; private set; }
        public EventFilter EventFilter { get; private set; }
        public EmoteTable EmoteTable { get; private set; }
        public List<CreationProfile> CreationProfiles { get; private set; }

        public PageDataList PageDataList { get; private set; }
        public GeneratorTable GeneratorTable { get; private set; }
        public GeneratorRegistry GeneratorRegistry { get; private set; }
        public GeneratorQueue GeneratorQueue { get; private set; }

        public WClassIDEnum WeenieClass { get; private set; }

        public Qualities()
        {

        }

        public new Qualities Unpack(BinaryReader reader)
        {
            base.Unpack(reader);

            uint qualFlags = reader.ReadUInt32();
            WeenieClass = (WClassIDEnum)reader.ReadUInt32();

            if ((qualFlags & 1) != 0)
            {
                AttributeCache = new AttributeCache();
                AttributeCache.Unpack(reader);
            }

            if ((qualFlags & 2) != 0)
            {
                Skills = new Dictionary<SkillList, Skill>();

                uint skillCount = reader.ReadUInt16();
                uint skillSize = reader.ReadUInt16();

                for (int s = 0; s < skillCount; s++)
                {
                    Skills.Add((SkillList)reader.ReadUInt32(), new Skill().Unpack(reader));
                }
            }

            if ((qualFlags & 0x4) != 0)
            {
                Body = new Body().Unpack(reader);
            }

            if ((qualFlags & 0x100) != 0)
            {
                SpellBook = new SpellBook().Unpack(reader);
            }

            if ((qualFlags & 0x200) != 0)
            {
                Enchantments = new EnchantmentRegistry().Unpack(reader);
            }

            if ((qualFlags & 0x8) != 0)
            {
                EventFilter = new EventFilter().Unpack(reader);
            }

            if ((qualFlags & 0x10) != 0)
            {
                EmoteTable = new EmoteTable().Unpack(reader);
            }

            if ((qualFlags & 0x20) != 0)
            {
                CreationProfiles = new List<CreationProfile>();
                uint numCreationProf = reader.ReadUInt16();
                uint sizeCreationProf = reader.ReadUInt16();

                for (int cf = 0; cf < numCreationProf; cf++)
                {
                    CreationProfiles.Add(new CreationProfile().Unpack(reader));
                }
            }

            if ((qualFlags & 0x40) != 0)
            {
                PageDataList = new PageDataList().Unpack(reader);
            }

            if ((qualFlags & 0x80) != 0)
            {
                GeneratorTable = new GeneratorTable().Unpack(reader);
            }

            if ((qualFlags & 0x400) != 0)
            {
                GeneratorRegistry = new GeneratorRegistry().Unpack(reader);
            }

            if ((qualFlags & 0x800) != 0)
            {
                GeneratorQueue = new GeneratorQueue().Unpack(reader);
            }

            return this;
        }

        public new void Pack(BinaryWriter writer)
        {
            base.Pack(writer);

            uint qualFlags = 0;

            if (AttributeCache != null)
            {
                qualFlags |= 1;
            }

            if (Skills != null && Skills.Count > 0)
            {
                qualFlags |= 2;
            }

            if (Body != null && Body.BodyPartTable.Count > 0)
            {
                qualFlags |= 4;
            }

            if (SpellBook != null && SpellBook.SpellList.Count > 0)
            {
                qualFlags |= 0x100;
            }

            if (Enchantments != null && (Enchantments.MultiEnchantments.Count > 0 || Enchantments.Additions.Count > 0 || Enchantments.Cooldowns.Count > 0 || Enchantments.Vitae != null))
            {
                qualFlags |= 0x200;
            }

            if (EventFilter != null && EventFilter.Events.Count > 0)
            {
                qualFlags |= 8;
            }

            if (EmoteTable != null && EmoteTable.EmoteSets.Count > 0)
            {
                qualFlags |= 0x10;
            }

            if (CreationProfiles != null)
            {
                qualFlags |= 0x20;
            }

            if (PageDataList != null && PageDataList.Pages.Count > 0)
            {
                qualFlags |= 0x40;
            }

            if (GeneratorTable != null && GeneratorTable.Profiles.Count > 0)
            {
                qualFlags |= 0x80;
            }

            if (GeneratorRegistry != null && GeneratorRegistry.Nodes.Count > 0)
            {
                qualFlags |= 0x400;
            }

            if (GeneratorQueue != null && GeneratorQueue.Nodes.Count > 0)
            {
                qualFlags |= 0x800;
            }

            writer.Write(qualFlags);
            writer.Write((uint)WeenieClass);

            if(AttributeCache != null)
                AttributeCache.Pack(writer);

            if (Skills != null)
            {
                writer.Write((ushort)Skills.Count);
                writer.Write((ushort)16);

                foreach (KeyValuePair<SkillList, Skill> skl in Skills)
                {
                    writer.Write((uint)skl.Key);
                    skl.Value.Pack(writer);
                }
            }

            if(Body != null && Body.BodyPartTable.Count > 0)
                Body.Pack(writer);

            if (SpellBook != null && SpellBook.SpellList.Count > 0)
                SpellBook.Pack(writer);

            if (Enchantments != null && (Enchantments.MultiEnchantments.Count > 0 || Enchantments.Additions.Count > 0 || Enchantments.Cooldowns.Count > 0 || Enchantments.Vitae != null))
                Enchantments.Pack(writer);

            if (EventFilter != null && EventFilter.Events.Count > 0)
                EventFilter.Pack(writer);

            if (EmoteTable != null && EmoteTable.EmoteSets.Count > 0)
                EmoteTable.Pack(writer);

            if (CreationProfiles != null)
            {
                writer.Write(CreationProfiles.Count);
                
                foreach (CreationProfile cp in CreationProfiles)
                    cp.Pack(writer);
            }

            if (PageDataList != null && PageDataList.Pages.Count > 0)
                PageDataList.Pack(writer);

            if (GeneratorTable != null && GeneratorTable.Profiles.Count > 0)
                GeneratorTable.Pack(writer);

            if (GeneratorRegistry != null && GeneratorRegistry.Nodes.Count > 0)
                GeneratorRegistry.Pack(writer);

            if (GeneratorQueue != null && GeneratorQueue.Nodes.Count > 0)
                GeneratorQueue.Pack(writer);

        }
    }
}
