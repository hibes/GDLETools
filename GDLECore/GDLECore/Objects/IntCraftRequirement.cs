﻿using GDLECore;

using System;
using System.IO;

namespace GDLECore
{
    public class IntCraftRequirement : IPackable<IntCraftRequirement>
    {
        public PropertyInt Property;
        public int Value;
        public uint Operation;  // TODO: Change to enum
        public string Message;

        public void Pack(BinaryWriter writer)
        {
            throw new NotImplementedException();
        }

        public IntCraftRequirement Unpack(BinaryReader reader)
        {
            Property = (PropertyInt)reader.ReadUInt32();
            Value = reader.ReadInt32();
            Operation = reader.ReadUInt32();
            Message = reader.ReadGDLEString();

            return this;
        }
    }
}
