﻿using GDLECore;

using System;
using System.IO;

namespace GDLECore
{
    public class FloatCraftRequirement : IPackable<FloatCraftRequirement>
    {
        public PropertyFloat Property;
        public double Value;
        public uint Operation;  // TODO: Change to enum
        public string Message;

        public void Pack(BinaryWriter writer)
        {
            throw new NotImplementedException();
        }

        public FloatCraftRequirement Unpack(BinaryReader reader)
        {
            Property = (PropertyFloat)reader.ReadUInt32();
            Value = reader.ReadDouble();
            Operation = reader.ReadUInt32();
            Message = reader.ReadGDLEString();

            return this;
        }
    }
}
