﻿using GDLECore;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDLECore
{
    public class FloatCraftMod : IPackable<FloatCraftMod>
    {
        public int UnknownInt;
        public int Operation; // TODO: Make enum
        public PropertyFloat Property;
        public double Value;

        public void Pack(BinaryWriter writer)
        {
            throw new NotImplementedException();
        }

        public FloatCraftMod Unpack(BinaryReader reader)
        {
            UnknownInt = reader.ReadInt32();
            Operation = reader.ReadInt32();
            Property = (PropertyFloat)reader.ReadInt32();
            Value = reader.ReadDouble();
            return this;
        }
    }
}
