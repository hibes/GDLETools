﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDLECore
{
    public class TextureMapChange : IPackable<TextureMapChange>
    {
        public uint PartIndex { get; private set; }
        public uint OldTextureId { get; private set; }
        public uint NewTextureId { get; private set; }

        public TextureMapChange()
        { }

        public TextureMapChange Unpack(BinaryReader reader)
        {
            PartIndex = reader.ReadByte();
            OldTextureId = reader.UnpackFromUnknown(0x05000000);
            NewTextureId = reader.UnpackFromUnknown(0x05000000);

            return this;
        }

        public void Pack(BinaryWriter writer)
        {
            writer.Write((byte)PartIndex);
            writer.PackToUnknown(0x05000000, OldTextureId);
            writer.PackToUnknown(0x05000000, NewTextureId);
        }

    }
}
