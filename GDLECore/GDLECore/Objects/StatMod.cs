﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDLECore
{
    public class StatMod : IPackable<StatMod>
    {
        public uint Type { get; private set; }
        public uint Key { get; private set; }
        public float Value { get; private set; }

        public StatMod()
        { }

        public StatMod Unpack(BinaryReader reader)
        {
            Type = reader.ReadUInt32();
            Key = reader.ReadUInt32();
            Value = reader.ReadSingle();
            return this;
        }

        public void Pack(BinaryWriter writer)
        {
            writer.Write(Type);
            writer.Write(Key);
            writer.Write(Value);
        }
    }
}
