﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDLECore
{
    public enum BODY_HEIGHT
    {
        UNDEF_BODY_HEIGHT = 0x0,
        HIGH_BODY_HEIGHT = 0x1,
        MEDIUM_BODY_HEIGHT = 0x2,
        LOW_BODY_HEIGHT = 0x3,
        NUM_BODY_HEIGHTS = 0x4,
        FORCE_BODY_HEIGHT32_BIT = 0x7FFFFFFF,
    };
}
