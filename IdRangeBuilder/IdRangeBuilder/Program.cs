﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdRangeBuilder
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Creating Tables for unused IDs");
            Database.CreateConnectionString();
            Database.ConnectAndOpen();
            Database.ReCreateIDTable();

            UInt32 startRange = uint.Parse(System.Configuration.ConfigurationManager.AppSettings["startRange"].ToString().Trim());
            string foo = System.Configuration.ConfigurationManager.AppSettings["endRange"].ToString().Trim();
            string bar = UInt32.MaxValue.ToString();
            UInt32 endRange = UInt32.Parse(bar);
            int rangeGroup = int.Parse(System.Configuration.ConfigurationManager.AppSettings["selectRange"].ToString().Trim());
            UInt32 currentStart = startRange;

            while (currentStart + rangeGroup < endRange)
            {
                Console.WriteLine("StartRange:" + currentStart);
                StringBuilder sb = new StringBuilder();
                sb.Append("INSERT INTO idranges VALUES");
                List<UInt32> ranges = Database.GetNextRange(currentStart, rangeGroup);

                for (uint x = currentStart; x < currentStart + rangeGroup; x++)
                {
                    if (!ranges.Contains(x))
                    {
                        sb.Append("(");
                        sb.Append(x.ToString());
                        sb.Append("),");
                    }
                }

                string insertQuery = sb.ToString().TrimEnd(',') + ";";

                Database.InsertRange(insertQuery);

                currentStart += (uint)rangeGroup;

            }

        }

    }
}
