﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDFrags
{
    class Program
    {
        static void Main(string[] args)
        {
            // Connect to DB
            Database.CreateConnectionString();
            Database.ConnectAndOpen();
            // Get highest ID in weenies
            UInt32 maxWeenieID = Database.GetHigestID();

            Random random = new Random(DateTime.Now.Millisecond);

            UInt32 currentID = maxWeenieID;
            while (currentID < UInt32.MaxValue)
            {
                currentID += (uint)random.Next(1000, 10000);

                int numToAdd = random.Next(1, 15);
                StringBuilder sb = new StringBuilder();
                sb.Append("INSERT INTO weenies (id, top_level_object_id, block_id, data) VALUES");
                for (int x = 0; x < numToAdd; x++)
                {
                    currentID++;
                    sb.Append("(");
                    sb.Append(currentID.ToString());
                    sb.Append(",");
                    sb.Append(currentID.ToString());
                    sb.Append(",0,'0x07'),");
                }

                string insertQuery = sb.ToString().TrimEnd(',') + ";";

                Database.InsertNewWeenieRange(insertQuery);

            }

        }
    }
}
